package Falabella.Pausa;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Pausa {
    private WebDriver driver;

    public Pausa(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void Espera(Integer intTiempo)
    {
        driver.manage().timeouts().implicitlyWait(intTiempo, TimeUnit.SECONDS);
    /*WebDriverWait Tiempo = new WebDriverWait();
    Tiempo.until() */
    }

    public void evidencia(String strFuncionalidad, String strPasoCaso){
        String rutaReporte = " ";
        rutaReporte = System.getProperty("user.dir")+"\\reportes\\"+strPasoCaso+".png";
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try{
            FileUtils.copyFile(scrFile, new File(rutaReporte));
            System.out.println("[Evidencia] : "+ strPasoCaso+".png"+"  Captura Exitosa" + strFuncionalidad);

        }catch (IOException e) {
            e.printStackTrace();
            System.out.println("[Evidencia] : "+ strPasoCaso+".png"+"  Captura Fallida " + strFuncionalidad);

        }
    }


}

