/* --------------------------------------------------
   Se declaran los elementos a utilizar en la página:
  ---------------------------------------------------*/

    /* @FindBy(xpath = "//*[@id='iframeContenido'][@onload='fnCargaIframe();']");
       private WebElement nombreEmpresa; //)
        @FindBy(className = "StarsEventWatcher") //div[@class='StarsEventWatcher']
        private WebElement estrellasEncuesta; //Estrellas a seleccionar en Qualtrics (encuesta)
    */

package Pages;

//import Falabella.Pausa.Pausa;

import Falabella.Pausa.Pausa;
import driver.DriverContext;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static accionesGenericas.ControlledActions.*;
import static accionesGenericas.Utils.esperar;

public class BusquedaProductosFalabella {
    private WebDriver driver;
    private Pausa ObjPausa;

    //  ----------------------------------   Constructor:   ----------------------------------
    public BusquedaProductosFalabella() {

        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this );
    }
    //  ----------------------------------   Constructor:   ----------------------------------

    /* -----------------------------------------
       Declara elementos de la página 2 Inicio
       ----------------------------------------- */

    //   2.- Ir a “CATEGORÍAS”, luego “Tecnología”, y hacer clic en “Consolas” (Videojuegos)
    @FindBy(xpath = "//*[@class='HamburgerIcon_hamburgerBox__f-nlG']") private WebElement btnBuscarCategoria; // Categorias

    @FindBy(xpath = "//*[@id=\"item-3\"]") private WebElement txtTecnologia;                          // Tecnologia
    @FindBy(xpath = "//*[contains(text(),'Tecnología') and @class= 'FirstLevelItems_menuText__UYB9A']") private WebElement btnTecnologia;                          // Tecnologia
    @FindBy(xpath = "//a[contains(text(),'Consola')]") private WebElement lnkConsolas;             // Textbox selecciona consola

    //   3.- Filtrar la búsqueda por “Marca” (Seleccionar una sola Marca)
    @FindBy(xpath = "//*[@id=\"testId-Accordion-Marca\"]") private WebElement txtHabMarca;            // Textbox habilita marca
    @FindBy(xpath = "//*[@id=\"testId-Multiselect-Marca\"]") private WebElement txtBuscaMarca;        // Textbox escribe marca
    @FindBy(xpath = "//*[contains(text(),\"generico\")]") private WebElement txtSelMarca;             // Textbox selecciona marca

    //   4.- Ver el detalle de cualquier producto (hacer clic en el botón “VER PRODUCTO”)
    @FindBy(xpath = "//*[contains(text(),\"VER PRODUCTO\")]") private WebElement btnVerDetalle;       // Botón ver detalle producto

    //   5.- Aumentar la cantidad hasta 3 (hacer clic en el botón “+” hasta llegar a 3)
    @FindBy(xpath = "//*[@id=\"buttonForCustomers\"]") private WebElement btnBolsaProducto;             // Botón Bolsa producto
    @FindBy(xpath = "//*[contains(@class,\'jsx-635184967 increment\')]") private WebElement btnAumentaProducto;      // Botón Aumenta producto
    @FindBy(xpath = "//*[contains(text(),\"Ver Bolsa de Compras\")]") private WebElement btnBolsatProducto;             // Textbox selecciona marca


    /*-------------------------------------------------------
      Métodos asociados a la page validad pruducto INICIO
     --------------------------------------------------------*/

    public void validaProductos(){

        /* -----------------------------------------------------------------------------------
           2.- Ir a “CATEGORÍAS”, luego “Tecnología”, y hacer clic en “Consolas” (Videojuegos)
           ----------------------------------------------------------------------------------- */

        if (exists(btnBuscarCategoria))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(btnBuscarCategoria ,30);
        btnBuscarCategoria.click();

        if (exists(btnTecnologia))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(btnTecnologia,30);
        btnTecnologia.click();

        if (exists(lnkConsolas))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(lnkConsolas ,10);
        evidencia("CPA00001.PASO 02:  Ir a CATEGORÍAS luego Tecnología haciendo clic en Consolas (Videojuegos)","CPA00001.Paso02BusquedaProducto" );
        lnkConsolas.click();


        /* -----------------------------------------------------------------------------------
           3.- Filtrar la búsqueda por “Marca” (Seleccionar una sola Marca GENERICO)
           ----------------------------------------------------------------------------------- */
        if (exists(txtHabMarca))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(txtHabMarca ,5);
        txtHabMarca.click();

        txtBuscaMarca.sendKeys("GENERICO");
        txtBuscaMarca.sendKeys( Keys.ENTER );
        esperar(01);
        if (exists(txtSelMarca))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(txtSelMarca ,5);
        txtSelMarca.click();

        evidencia("CPA00001.PASO 03: Filtrar la búsqueda por Marca (Seleccionar una sola Marca GENERICO","CPA00001.Paso03BusquedaProducto" );

        esperar(01);

        /* -----------------------------------------------------------------------------------
           4.- Ver el detalle de cualquier producto (hacer clic en el botón “VER PRODUCTO”)
           ----------------------------------------------------------------------------------- */
        if (exists(btnVerDetalle))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(btnVerDetalle ,5);
        btnVerDetalle.click();

        evidencia("CPA00001.PASO 04: Ver el detalle de cualquier producto","CPA00001.Paso04DetalleProducto" );

        esperar(01);

        if (exists(btnBolsaProducto))  {
            Assert.assertFalse( false );
        }
        visualizarObjeto(btnBolsaProducto ,10);
        btnBolsaProducto.click();
        esperar(20);


        /* -----------------------------------------------------------------------------------
           5.- Aumentar la cantidad hasta 3 (hacer clic en el botón “+” hasta llegar a 3)
           ----------------------------------------------------------------------------------- */
        for (int j = 0; j < 2; j++) {
            btnAumentaProducto.click();
            esperar(02);
        }
        visualizarObjeto(btnBolsatProducto ,5);
        btnBolsatProducto.click();
        esperar(05);
        evidencia("CPA00001.PASO 05: Aumentar la cantidad hasta 3 del producto","CPA00001.Paso04AumentarCantidad" );

    }
    /*-------------------------------------------------------
      Métodos asociados a la page validad pruducto TERMINO
     --------------------------------------------------------*/
}
