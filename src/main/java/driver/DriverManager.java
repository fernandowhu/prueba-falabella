package driver;

import constants.Navegador;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;


public class DriverManager {
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebDriver webDriver;
    private File root = new File("driverNavegador");
    private String extensionDriver = "";

    protected void resolveDriver(Navegador nav, String ambURL) {
        resolveDriver(nav, ambURL, "", false, false,"");
    }

    protected void resolveDriver(Navegador nav, String ambURL, String nombreDriverVersion) {
        resolveDriver(nav, ambURL, "", false, false, nombreDriverVersion);
    }

    protected void resolveDriver(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador) {
        resolveDriver(nav, ambURL, archivoExtension, extensionesNavegador, false,"");
    }

    protected void resolveDriver(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador, boolean argNoSandbox) {
        resolveDriver(nav, ambURL, archivoExtension, extensionesNavegador, argNoSandbox,"");
    }

    protected void resolveDriver(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador, boolean argNoSandbox, String nombreDriverVersion) {
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println("\nSistema operativo ->" + System.getProperty("os.name").toLowerCase() + "\n");
        File driverPath, cspPath;
        //windows o mac
        if (!(os.contains("mac") || os.contains("linux"))) {
            extensionDriver = ".exe";
        }

        switch (nav) {
            case Chrome:
                System.out.println("Se selecciona Chrome");
                if (!nombreDriverVersion.trim().equals("")) {
                    driverPath = new File(root, nombreDriverVersion + extensionDriver);
                } else {
                    driverPath = new File(root, "chromedriver" + extensionDriver);
                }
                System.setProperty("webdriver.chrome.driver", driverPath.getAbsolutePath());
                ChromeOptions options = new ChromeOptions();
                if(argNoSandbox){
                    options.addArguments(" -no-sandbox"); //Para forzar al navegador de Chrome ejecutar
                }
                if (extensionesNavegador) { //Para agregar una extensión al navegador

                    cspPath = new File(root, archivoExtension);
                    options.addExtensions(cspPath.getAbsoluteFile());
                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                    options.merge(capabilities);
                }
                webDriver = new ChromeDriver(options);

                capabilities.setBrowserName("Chrome");
                webDriver.manage().window().maximize();

                break;
            case Explorer:
                System.out.println("Se selecciona Explorer");
                if (!nombreDriverVersion.trim().equals("")) {
                    driverPath = new File(root, nombreDriverVersion + extensionDriver);
                } else {
                    driverPath = new File(root, "IEDriverServer" + extensionDriver);
                }
                System.setProperty("webdriver.ie.driver", driverPath.getAbsolutePath());
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
                webDriver = new InternetExplorerDriver();
                capabilities.setBrowserName("Explorer");
                webDriver.manage().window().maximize();
                break;
            case Firefox:
                System.out.println("Se selecciona Firefox");
                if (!nombreDriverVersion.trim().equals("")) {
                    driverPath = new File(root, nombreDriverVersion + extensionDriver);
                } else {
                    driverPath = new File(root, "geckodriver" + extensionDriver);
                }
                System.setProperty("webdriver.gecko.driver", driverPath.getAbsolutePath());
                webDriver = new FirefoxDriver();
                capabilities.setBrowserName("Firefox");
                webDriver.manage().window().maximize();
                break;
            default:
                System.out.println("No es posible lanzar el navegador, no se reconoce el navegador: " + nav);
                ////reporte("resolveDriver", "No se reconoce el navegador: " + nav, EstadoPrueba.FAILED, true);

        }

        ////reporte("resolveDriver", "Selecciona el navegador: "+nav, EstadoPrueba.PASSED, false);
        webDriver.get(ambURL);
    }

    protected WebDriver getDriver() {
        return webDriver;
    }

    protected void setDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    protected Dimension getScreenSize() {
        return webDriver.manage().window().getSize();
    }
}
