package driver;

import constants.Navegador;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

//import java.sql.DriverManager;


public class DriverContext {

    private static DriverManager driverManager = new DriverManager();
    private static Navegador tipoNavegador;

    public static String getAmbienteURL() {
        return ambienteURL;
    }

    public static void setAmbienteURL(String ambienteURL) {
        DriverContext.ambienteURL = ambienteURL;
    }

    private static String ambienteURL = "";

    public static String getTipoNavegador() {
        return tipoNavegador.toString();
    }

    public static void setTipoNavegador(Navegador tipoNavegador) {
        DriverContext.tipoNavegador = tipoNavegador;
    }

    public static void setUp(Navegador nav, String ambURL) {
        setTipoNavegador(nav);
        setAmbienteURL(ambURL);
        System.out.println("driver context");
        driverManager.resolveDriver(nav, ambURL);
    }

    public static void setUp(Navegador nav, String ambURL, String nombreDriverVersion) {
        setTipoNavegador(nav);
        setAmbienteURL(ambURL);
        System.out.println("driver context");
        driverManager.resolveDriver(nav, ambURL, nombreDriverVersion);
    }

    public static void setUp(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador) {
        setTipoNavegador(nav);
        setAmbienteURL(ambURL);
        System.out.println("driver context con extensiones del navegador");
        driverManager.resolveDriver(nav, ambURL, archivoExtension, extensionesNavegador);
    }

    public static void setUp(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador, boolean argNoSandbox) {
        setTipoNavegador(nav);
        setAmbienteURL(ambURL);
        System.out.println("driver context con extensiones del navegador");
        driverManager.resolveDriver(nav, ambURL, archivoExtension, extensionesNavegador, argNoSandbox);
    }

    public static void setUp(Navegador nav, String ambURL, String archivoExtension, boolean extensionesNavegador, boolean argNoSandbox, String nombreDriverVersion) {
        setTipoNavegador(nav);
        setAmbienteURL(ambURL);
        System.out.println("driver context con extensiones del navegador");
        driverManager.resolveDriver(nav, ambURL, archivoExtension, extensionesNavegador, argNoSandbox, nombreDriverVersion);
    }

    public static WebDriver getDriver() {
        return driverManager.getDriver();
    }

    public static void setDriverTimeout(Integer tiempo) {
        driverManager.getDriver().manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);
    }

    public static void quitDriver() {
        driverManager.getDriver().quit();
    }

    public static Dimension getSreenSize() {
        return driverManager.getScreenSize();
    }

}
