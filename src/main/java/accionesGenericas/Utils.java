package accionesGenericas;

import driver.DriverContext;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.right;

public class Utils {


    /***************************************************
     * Metodo que valida si se ha cargado la pantalla. *
     ***************************************************/
    public static void ingresoPantalla(WebElement objeto, String tituloPantalla, String tipoElemento) {
        ControlledActions.loadingNegado(objeto, 60, "No ingresa a la pantalla esperada: " + tituloPantalla, tipoElemento); //existeElemento
        //Reporte("Ingreso a Pantalla", "Se ingresa correctamente a la pantalla " + tituloPantalla, EstadoPrueba.PASSED, false);
    }
    
    public static Integer limpiarNumeros(String valor) {
        Integer monto;
        try {
            if (StringUtils.isEmpty(valor)) {
                throw new IllegalArgumentException();
            }
            monto = Integer.parseInt(valor.replaceAll("\\D+", ""));
        } catch (IllegalArgumentException ex) {
            ////reporte("Limpiar Numeros", "[Utils.limpiarNumeros] Se entregó un string vacio", EstadoPrueba.FAILED, true);
            return null;
        }
        return monto;
    }

    @Step("Validación de texto")
    public static boolean validarTexto(String textoApp, String textoAValidar) {
        if (textoApp.trim().equals(textoAValidar.trim())) {
            ////reporte("Validación de texto", "Texto:" + textoApp + " validado segun diseño y es correcto", EstadoPrueba.PASSED, false);
            return true;
        } else {
            ////reporte("Validación de texto", "Texto desplegado en app:" + textoApp + " validado segun diseño y no es correcto, deberia desplegar:" + textoAValidar, EstadoPrueba.FAILED, false);
            return false;
        }
    }


    /*
        ESTE METODO ES EL MISMO QUE EL DE ARRIBA SOLO QUE TRABAJA EL OBJETO COMPLETO PARA SU VALIDACION QUE SEGÚN
        LO TRABAJADO DEBERIA SER EL IDEAL
     */
    @Step("Validación de objeto y texto")
    public static void reporteValidacionTextos(WebElement objeto, String textoEsperado) {
        try {
            boolean existeObjeto = ControlledActions.visualizarObjeto(objeto, 5);
            if (existeObjeto == true) {
                //PdfxxxReports.addTextValidate("Validación de objeto y texto", textoEsperado, objeto.getText().trim().replace("\n", " "), false);
            } else {
                ////reporte("Validación de objeto y texto", "No se visualiza el texto:'" + textoEsperado + "' en la vista desplegada.", EstadoPrueba.FAILED, false);
            }
        } catch (Exception e) {
            System.out.println("No se  logra ejecutar funcion reporteValidacionTextos, motivo:" + e.getMessage());
            ////reporte("Validación de objeto y texto", "Error en el método 'reporteValidacionTextos', motivo:" + e.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //no lleva step por que no reporta
    public static boolean validarTextoSinReporte(String textoApp, String textoAValidar) {
        if (textoApp.trim().equals(textoAValidar.trim())) {
            return true;
        } else {
            return false;
        }
    }

    //no lleva step por que no reporta
    public static boolean isEnabled(WebElement element) throws NoSuchElementException {
        System.out.println("Esta el elemento habilitado?:" + element.getAttribute("enabled"));
        return element.getAttribute("enabled").trim().equals("true");
    }

    /***
     * Valida si una cadena es numerica
     * @param cadena es un string
     * @return
     */
    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    /******/
    public static byte[] getCaptura(String datosBD, int heightImagen) throws Exception {

        HashMap<RenderingHints.Key, Object> renderingProperties = new HashMap<>();

        //String screenText = StringUtils.join(s.readScreen(), "\n");
        renderingProperties.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderingProperties.put(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        renderingProperties.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

        Font font = new Font("Consolas", Font.PLAIN, 12);
        FontRenderContext fontRenderContext = new FontRenderContext(null, true, true);

        BufferedImage bufferedImage = new BufferedImage(600, heightImagen, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setRenderingHints(renderingProperties);
        graphics2D.setBackground(Color.black);
        graphics2D.setColor(Color.white);
        graphics2D.setFont(font);
        graphics2D.clearRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());
        TextLayout textLayout = new TextLayout(datosBD, font, fontRenderContext);

        Double cont = 0.0;
        for (String line : datosBD.split(",")) {
            graphics2D.drawString(line, 15, (int) (15 + cont * textLayout.getBounds().getHeight()));
            cont = cont + 1.5;
        }

        graphics2D.dispose();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "PNG", out);
        return out.toByteArray();
    }



    public static String[] obtenerArregloCadenaEspacios(String cadena) {
        List<String> splittedString = Arrays.asList(cadena.split(" "));
        String registros = "";

        for (int i = 0; i < splittedString.size(); i++) {
            registros = registros + "|" + splittedString.get(i);
        }

        do {
            registros = registros.replace("||", "|");
        } while (registros.indexOf("||") >= 0);

        System.out.println(registros);
        String separador = Pattern.quote("|");
        return registros.split(separador);
    }

    /**
     * Convierte
     *
     * @param str
     * @return
     */
    public static String ucFirst(String str) {
        if (str.isEmpty()) {
            return str;
        } else {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }

    public static String getFechaActual() {
        LocalDate localDate = LocalDate.now();
        Locale spanishLocale = new Locale("es", "ES");
        String nameMonthActual = ucFirst(localDate.format(java.time.format.DateTimeFormatter.ofPattern("MMMM", spanishLocale)));
        String mes = DateFormater.getNumberFromMonth(nameMonthActual.trim());
        int dia = localDate.getDayOfMonth();
        int anio = localDate.getYear();
        String fecha = right("0" + dia, 2) + right("0" + mes, 2) + anio;
        return fecha;
    }

    /****
     * Método para ordenar una cadena alfabéticamente
     * Ejemplo:
     *  sBSABdfsabASF  -> AABBFSSabdfss
     * @param inputString  -> Cadena
     * @return
     */
    public static String ordenarCadena(String inputString) {
        //Convertir la cadena en un arreglo tipo 'char'
        char tempArray[] = inputString.toCharArray();

        //Ordenar el arreglo de forma alfabética
        Arrays.sort(tempArray);

        //Retorna la cadena ordenada alfabéticamente
        return new String(tempArray);
    }

    /****
     * Método para ordenar una cadena alfabéticamente
     * Ejemplo:
     *  sBSABdfsabASF  -> AaABBbdfFsSsS
     * @param inputString
     * @return
     */
    public static String ordenarCadenaPosicion(String inputString) {
        //Convertir la cadena en un arreglo tipo 'char'
        Character tempArray[] = new Character[inputString.length()];
        for (int i = 0; i < inputString.length(); i++) {
            tempArray[i] = inputString.charAt(i);
        }

        //Ordenar, ignorando el caso durante la clasificación
        Arrays.sort(tempArray, new Comparator<Character>() {
            @Override
            public int compare(Character c1, Character c2) {
                //Ignorando el caso
                return Character.compare(Character.toLowerCase(c1),
                        Character.toLowerCase(c2));
            }
        });

        //Usando StringBuilder para convertir la matriz de caracteres a String
        StringBuilder sb = new StringBuilder(tempArray.length);
        for (Character c : tempArray) {
            sb.append(c.charValue());
        }

        return sb.toString();
    }

    public static Double convertirDoubleString(String monto) {
        Double valor = 1.0;
        valor = Double.parseDouble(monto.trim().replace(".", "").replace(",", "."));
        return valor;
    }

    public static void mueveMouseAlElemento(WebElement element) {
        Actions action = new Actions( DriverContext.getDriver());
        action.moveToElement(element).build().perform();
    }

    public static void scrollFocusAlElemento(WebElement element) {
        ((JavascriptExecutor) DriverContext.getDriver()).executeScript("arguments[0].scrollIntoView();", element);
    }

    public static void mouseFocusElemento(WebElement element) {
        String javaScript = "var evObj = document.createEvent('MouseEvents');"
                + "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
                + "arguments[0].dispatchEvent(evObj);";
        ((JavascriptExecutor) DriverContext.getDriver()).executeScript(javaScript, element);
    }

    //Resalta el objeto a través de los estilos css
    public static void enmarcarObjeto(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) DriverContext.getDriver();
            for (int i = 0; i < 3; i++) {
                js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 5px solid LimeGreen;");
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            ////reporte("enmarcarObjeto", "Error: Al ejecutar JavaScript para modificar propiedad css del elemento. " + e.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    public static void desenmarcarObjeto(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) DriverContext.getDriver();
            for (int i = 0; i < 3; i++) {
                Thread.sleep(500);
                js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
                        element, "");
            }
        } catch (InterruptedException e) {
            ////reporte("desenmarcarObjeto", "Error: Al ejecutar JavaScript para modificar propiedad css del elemento. " + e.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //Método para cambiar el Iframe a través de las propiedades del 'findElement'
    public static void cambiarIframe(String tipoLocalizador, String valorPropiedad) {
        System.out.println("\nMETODO ->'cambiarIframe'");
        DriverContext.getDriver().switchTo().defaultContent();
        switch (tipoLocalizador.toUpperCase()) {

            case "ID":
                DriverContext.getDriver().switchTo().frame((WebElement) DriverContext.getDriver().findElement(By.id(valorPropiedad)));
                break;
            case "XPATH":
                DriverContext.getDriver().switchTo().frame((WebElement) DriverContext.getDriver().findElement(By.xpath(valorPropiedad)));
                break;
            case "NAME":
                DriverContext.getDriver().switchTo().frame((WebElement) DriverContext.getDriver().findElement(By.name(valorPropiedad)));
                break;
            case "CLASS":
                DriverContext.getDriver().switchTo().frame((WebElement) DriverContext.getDriver().findElement(By.className(valorPropiedad)));
                break;
            default:
                DriverContext.getDriver().switchTo().frame((WebElement) DriverContext.getDriver().findElement(By.xpath(valorPropiedad)));
        }
    }


    public static int numeroRandom(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("El numero Máximo debe ser mas grande que el mínimo");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    //CONVIERTE EL THREAD SLEEP EN SEGUNDOS ADEMÁS AGREGA COMENTARIOS
    public static void esperar(int segundos) {
        System.out.println("Inicio Espera de " + segundos + " segundos");
        long sec = segundos * 1000;
        try {
            Thread.sleep(sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Fin Espera de " + segundos + " segundos");
    }

    public static void scrollVertical(String cantidadPixel, WebElement iFrame) {
        System.out.println("\n[scrollVertical] cantidadPixel: " + cantidadPixel);
        ((JavascriptExecutor) DriverContext.getDriver().switchTo().defaultContent()).executeScript("window.scrollBy(0," + cantidadPixel + ")");
        try {
            DriverContext.getDriver().switchTo().frame(iFrame);
        } catch (NullPointerException e) {
            System.out.println("Continua el flujo");
        }
    }

    public static void scrollHorizontal(String cantidadPixel, WebElement iFrame) {
        System.out.println("\n[scrollHorizontal] cantidadPixel: " + cantidadPixel);
        ((JavascriptExecutor) DriverContext.getDriver()).executeScript("window.scrollBy(0," + cantidadPixel + ")");
        try {
            DriverContext.getDriver().switchTo().frame(iFrame);
        } catch (NullPointerException e) {
            System.out.println("Continua el flujo");
        }
    }

    public static void imprimerConsolaMsjPositivo(String mensaje) {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_GREEN = "\u001B[32m";
        System.out.println(ANSI_GREEN + "\n============== " + mensaje + ANSI_RESET);
    }


    public static void imprimerConsolaMsjNegativo(String mensaje) {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_PURPLE = "\u001B[35m";
        System.out.println(ANSI_PURPLE + "\n============== " + mensaje + ANSI_RESET);
    }

/*
    Entrada String  : $ 10.990 (Oferta)
    Salida  Numeric : 10990
 */
    public static int obtenerArregloConversionNumeros(String strPrecio) {

        String a1 = strPrecio.replace("$","");
        String b1 = a1.replace("(Oferta)","");
        String c1 = b1.replace(".","");
        String aCaracteres = c1.replace(" ","");
//        System.out.println("aCaracteres.length() => " + aCaracteres.length());
        String sValor = "";
        int num=0;
        for (int i = 0; i < aCaracteres.length(); i++) {
//            System.out.println( "aCaracteres.substring[" + i + "," + num +"] ===> " + aCaracteres.substring( i,num+1 ) );
            for (int j = 0; i <= 9; j++) {
//              Pendiente COMPARAR si el caracter es un numero
                String strDigito = Integer.toString( j );
              if (  aCaracteres.substring( i,num+1).contains(strDigito ) ) {
                sValor = sValor + aCaracteres.substring( i, num + 1 );
//                System.out.println( "strValor ===> " + sValor );
                break;
              }
            }
            num = num + 1;
        }
        return Integer.valueOf(sValor);}
//      return Integer.parseInt(strValor);
}
