package accionesGenericas;

import constants.SwipeDirection;
import driver.DriverContext;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static accionesGenericas.Utils.scrollHorizontal;
import static accionesGenericas.Utils.scrollVertical;
import static org.apache.commons.lang3.StringUtils.right;


public class ControlledActions {

    public static boolean exists(WebElement element) {
        System.out.println("\nMétodo -> 'exists'\n");
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException ex) {
            return false;
        } catch (StaleElementReferenceException ex) {
            return false;
        }
    }

    //Ciclo de espera de 60 segundos con la condición positiva
    //Mientras exista el element
    //@Step("Carga de maximo de 60 segundos")
    public static void loading(WebElement element, int tiempo, String descripcion, String tipoElemento) {
        System.out.println("\nMétodo -> 'loading'\n");
        try {
            long tiempoStart = System.currentTimeMillis();
            long tiempoEnd = tiempoStart + (60 * 1000); // 60 seconds * 1000 ms/sec

            do {
                if (System.currentTimeMillis() >= tiempoEnd) {
                    ////reporte("loading", descripcion, EstadoPrueba.FAILED, true);
                }
                Duration.ofMillis(100);
            } while (visualizarObjeto(element, tiempo));

        } catch (NoSuchElementException ex) {
            ////reporte("loading", "[ControlledActions] Error: No se pudo encontrar elemento '" + tipoElemento + "' -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //Ciclo de espera de 60 segundos con la condición negada
    //Mientras no exista el element
    //@Step("Carga mientras un elemento NO exista")
    public static void loadingNegado(WebElement element, int tiempo, String descripcion, String tipoElemento) {
        System.out.println("\nMétodo -> 'loadingNegado'\n");
        try {
            long tiempoStart = System.currentTimeMillis();
            long tiempoEnd = tiempoStart + (60 * 1000); // 60 seconds * 1000 ms/sec

            do {
                if (System.currentTimeMillis() >= tiempoEnd) {
                    ////reporte("loading", descripcion, EstadoPrueba.FAILED, true);
                }
                Duration.ofMillis(100);
            } while (!visualizarObjeto(element, tiempo));

        } catch (NoSuchElementException ex) {
            ////reporte("loading", "[ControlledActions] Error: No se pudo encontrar elemento '" + tipoElemento + "' -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //Ciclo de espera de 60 segundos con la condición negada
    //Mientras no exista el element
    //@Step("Carga mientras un elemento NO exista")
    public static void loadingNegadoScroll(WebElement element, int tiempo, String descripcion, String tipoElemento) {
        System.out.println("\nMétodo -> 'loadingNegado'\n");
        try {
            long tiempoStart = System.currentTimeMillis();
            long tiempoEnd = tiempoStart + (60 * 1000); // 60 seconds * 1000 ms/sec

            do {
                if (System.currentTimeMillis() >= tiempoEnd) {
                    ////reporte("loading", descripcion, EstadoPrueba.FAILED, true);
                }
                Duration.ofMillis(100);
            } while (!visualizarObjetoScroll(element, tiempo));

        } catch (NoSuchElementException ex) {
            ////reporte("loading", "[ControlledActions] Error: No se pudo encontrar elemento '" + tipoElemento + "' -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //Método para hacer clik en cualquier botón
    public static void botonClick(WebElement element, String tipoBoton, boolean Imagen) {
        System.out.println("\nMétodo -> 'botonClick'\n");
        try {

            String reporteFailed = "Error: No se visualiza el botón de '" + tipoBoton + "'.";
            ControlledActions.loadingNegado(element, 3, reporteFailed, tipoBoton);
            Utils.enmarcarObjeto(element);
            if (element.isEnabled()) {
                if (Imagen) {
                    ////reporte("Click en el botón", "[ControlledActions] Se visualiza el botón de '" + tipoBoton + "', activo.", EstadoPrueba.PASSED, false);
                } else {
                    ////reporte("Click en el botón", "[ControlledActions] Se visualiza el botón de '" + tipoBoton + "', activo.", EstadoPrueba.PASSED, false);
                }
            } else {
                if (Imagen) {
                    ////reporte("Click en el botón", "[ControlledActions] Se visualiza el botón de '" + tipoBoton + "', inactivo", EstadoPrueba.FAILED, false);
                } else {
                    ////reporte("Click en el botón", "[ControlledActions] Se visualiza el botón de '" + tipoBoton + "', inactivo", EstadoPrueba.FAILED, false);
                }
            }
            Utils.desenmarcarObjeto(element);
            element.click();

        } catch (NoSuchElementException ex) {
            ////reporte("Click en el botón", "[ControlledActions] Error: No se pudo encontrar elemento '" + tipoBoton + "' -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    //Método para hacer clik en cualquier elemento
    public static void elementoClick(WebElement element, String descTipoElemento, String vista, boolean Imagen) {
        System.out.println("\nMétodo -> 'elementoClick'\n");
        try {

            String reporteFailed = "Error: Después de 60 segundos el objeto '" + descTipoElemento + "', no se visualiza correctamente en la vista de '" + vista + "'.";
            ControlledActions.loadingNegado(element, 3, reporteFailed, descTipoElemento);
            Utils.mueveMouseAlElemento(element);
            Utils.enmarcarObjeto(element);
            if (Imagen) {
                ////reporte("Click en el elemento: " + descTipoElemento, "[ControlledActions] El objeto '" + descTipoElemento + "', se visualiza correctamente en la vista de '" + vista + "'.", EstadoPrueba.PASSED, false);
            } else {
                ////reporte("Click en el elemento: " + descTipoElemento, "[ControlledActions] El objeto '" + descTipoElemento + "', se visualiza correctamente en la vista de '" + vista + "'.", EstadoPrueba.PASSED, false);
            }
            Utils.desenmarcarObjeto(element);
            element.click();

        } catch (NoSuchElementException ex) {
            ////reporte("Click en el elemento: " + descTipoElemento, "[ControlledActions] Error: No se pudo encontrar elemento: '" + descTipoElemento + "' asociado a la vista de '" + vista + "'. -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }


    public static void setAttribute(WebElement element, String attName, String attValue) {
        System.out.println("\nMétodo -> 'setAttribute'\n");
        JavascriptExecutor js = (JavascriptExecutor) DriverContext.getDriver();
        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);

    }

    public static void reporteElementoDesplegado(WebElement element, String descTipoElemento, String vista, String nombreAtributo, String valorAtributo) {
        System.out.println("\nMétodo -> 'reporteElementoDesplegado'\n");
        String reporteFailed = "Error: Después de 60 segundos el elemento '" + descTipoElemento + "', no se visualiza correctamente en la vista de '" + vista + "'.";
        ControlledActions.loadingNegado(element, 5, reporteFailed, "'" + descTipoElemento + "'");
        ////reporte("Elemento desplegado", "[ControlledActions] El objeto '" + descTipoElemento + "', se visualiza correctamente en la vista de '" + vista + "'.", EstadoPrueba.PASSED, false);

        System.out.println("\n" + nombreAtributo + " [Antes]: " + element.getAttribute(nombreAtributo) + "\n");
        setAttribute(element, nombreAtributo, valorAtributo);

        ControlledActions.loadingNegado(element, 5, reporteFailed, "'" + descTipoElemento + "'");
        System.out.println("\n" + nombreAtributo + " [Cambio]: " + element.getAttribute(nombreAtributo) + "\n");

    }


    //Ciclo de espera de 60 segundos con la condición negada
    //Mientras no exista los elementos
    //@Step("Carga mientras los elementos NO exista")
    public static void esperaPorDosElementoEnPantalla(WebElement element1, WebElement element2, int tiempo, String descripcion, String tiposElementos) {
        System.out.println("\nMétodo -> 'esperaPorDosElementoEnPantalla'\n");
        try {
            long tiempoStart = System.currentTimeMillis();
            long tiempoEnd = tiempoStart + 60 * 1000; // 60 seconds * 1000 ms/sec

            do {
                if (System.currentTimeMillis() >= tiempoEnd) {
                    ////reporte("esperaPorDosElementoEnPantalla", descripcion, EstadoPrueba.FAILED, true);
                }
                Duration.ofMillis(100);
            } while (!(visualizarObjeto(element1, tiempo) && visualizarObjeto(element2, tiempo)));

        } catch (NoSuchElementException ex) {
            ////reporte("esperaPorDosElementoEnPantalla", "[ControlledActions] Error: No se pudo encontrar los elemento '" + tiposElementos + "' -> " + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    /**************************************************************************************************
     * El metodo "visualizarObjeto" se encarga de validar que se visualice el objeto que le indiquemos,
     * para esto recibe el parametro element(objeto que caracteriza la vista), segundos(cuantos segundos le
     * daremos para que espere a encontrar el objeto), driver.
     **************************************************************************************************/
    public static boolean visualizarObjeto(WebElement objeto, int segundos) {
        try {
            System.out.println("Buscamos el objeto:" + objeto + ", esperamos " + segundos + " segundos, hasta que aparezca.");
            WebDriverWait wait = new WebDriverWait( DriverContext.getDriver(), segundos);
            wait.until( ExpectedConditions.visibilityOf(objeto));
            System.out.println("Se encontró objeto:" + objeto + ", se retorna true.");
            Utils.mueveMouseAlElemento(objeto);
            return true;
        } catch (Exception e) {
            System.out.println("No se encontró objeto:" + objeto + ", se retorna false.");
            return false;
        }
    }

    /**
     * recibe un elemento y un tiempo en segundos, para buscar el elemento, al encontrarlos valida si el objeto esta
     * con la propiedad "isEnabled" en true, retorna true o false
     *
     * @param objeto
     * @param segundos
     * @return
     */
    public static boolean validarEnable(WebElement objeto, int segundos) {
        System.out.println("Se validará que el objeto: " + objeto + " se encuentre enabled en " + segundos + " segundos.");
        int milisegundos = segundos * 1000;
        boolean res = false;
        for (int i = 0; i < 9; i++) {
            if (Utils.isEnabled(objeto)) {
                System.out.println("El objeto:" + objeto + " se encuentra enabled.");
                res = true;
                break;
            } else if (i == 9) {
                System.out.println("El objeto:" + objeto + " después de " + segundos + " segundos no se encuentra enabled.");
                res = false;
            } else {
                try {
                    Thread.sleep(milisegundos);
                } catch (InterruptedException e) {
                    ////reporte("validarEnable", "El Sleep del método validar Enable falló, el motivo: " + e.getMessage(), EstadoPrueba.FAILED, true);
                    Assert.fail("El Sleep del metodo validar Enable falló, el motivo:" + e.getMessage());
                }
            }
        }
        return res;
    }

    /*************************************************************************************************
     *El metodo "reporteObjetoDesplegado" sin imagen, genera reportes en base a los parametros que necesita, estadoObjeto
     * es un valor boleano que indica si el objeto esta o no en la vista, objeto es el nombre del objeto
     * parta identificarlo en reporte y vista para indentificar la vista en la que estamos.
     ************************************************************************************************/
    public static void reporteObjetoDesplegado(boolean estadoObjeto, String objeto, String vista, boolean fatal) {
        if (estadoObjeto == true) {
            ////reporte("Elemento encontrado: " + objeto, "El objeto -> " + objeto + ", se visualiza correctamente en la vista de " + vista + ".", EstadoPrueba.PASSED, false);
        } else {
            ////reporte("Elemento no encontrado: " + objeto, "El objeto -> " + objeto + ", no se visualiza  en la vista de " + vista + ".", EstadoPrueba.FAILED, fatal);
        }

    }

    /*************************************************************************************************
     *El metodo "reporteObjetoDesplegadoImagen" con imagen, genera reportes en base a los parametros que necesita, estadoObjeto
     * es un valor boleano que indica si el objeto esta o no en la vista, objeto es el nombre del objeto
     * parta identificarlo en reporte y vista para indentificar la vista en la que estamos.
     ************************************************************************************************/
    public static void reporteObjetoDesplegadoImagen(boolean estadoObjeto, String objeto, String vista, boolean fatal) {
        if (estadoObjeto == true) {
            ////reporte("Elemento encontrado: " + objeto, "El objeto -> " + objeto + ", se visualiza correctamente en la vista de " + vista + ".", EstadoPrueba.PASSED, false);
        } else {
            ////reporte("Elemento no encontrado: " + objeto, "El objeto -> " + objeto + ", no se visualiza  en la vista de " + vista + ".", EstadoPrueba.FAILED, fatal);
        }

    }

    // ControlledActions.reporteObjetoDesplegado(ControlledActions.visualizarObjetoScroll(invertirMisAhorros, 5, SwipeDirection.DOWN, "Opción: Invertir mis ahorros",iframeContenido), "Opción: Invertir mis ahorros", primeraPregunta, false);


    public static void reporteObjetoDesplegadoScroll(int maxPixelBuscarElemento, WebElement elemento, int tiempo, SwipeDirection direccionDesplazamiento, String objeto, WebElement iFrame, String vista, boolean fatal) {
        boolean estadoObjeto = visualizarObjetoScroll(maxPixelBuscarElemento, elemento, tiempo, direccionDesplazamiento, objeto,iFrame);
        if (estadoObjeto == true) {
            ////reporte("Elemento encontrado: " + objeto, "El objeto -> " + objeto + ", se visualiza correctamente en la vista de " + vista + ".", EstadoPrueba.PASSED, false);
        } else {
            ////reporte("Elemento no encontrado: " + objeto, "El objeto -> " + objeto + ", no se visualiza  en la vista de " + vista + ".", EstadoPrueba.FAILED, fatal);
        }

    }

    public static void reporteObjetoDesplegadoImagenScroll(int maxPixelBuscarElemento, WebElement elemento, int tiempo, SwipeDirection direccionDesplazamiento, String objeto, WebElement iFrame, String vista, boolean fatal) {
        boolean estadoObjeto = visualizarObjetoScroll(maxPixelBuscarElemento, elemento, tiempo, direccionDesplazamiento, objeto,iFrame);
        if (estadoObjeto == true) {
            ////reporte("Elemento encontrado: " + objeto, "El objeto -> " + objeto + ", se visualiza correctamente en la vista de " + vista + ".", EstadoPrueba.PASSED, false);
        } else {
            ////reporte("Elemento no encontrado: " + objeto, "El objeto -> " + objeto + ", no se visualiza  en la vista de " + vista + ".", EstadoPrueba.FAILED, fatal);
        }

    }

    public static void reporteEnmarcacionObjImagen(WebElement elemento, boolean estadoObjeto, String objeto, String vista, boolean fatal) {
        if (estadoObjeto == true) {
            Utils.enmarcarObjeto(elemento);
            ////reporte("Elemento encontrado: " + objeto, "El objeto:" + objeto + ", se visualiza correctamente en la vista de " + vista + ".", EstadoPrueba.PASSED, false);
            Utils.desenmarcarObjeto(elemento);
        } else {
            ////reporte("Elemento no encontrado: " + objeto, "El objeto:" + objeto + ", no se visualiza  en la vista de " + vista + ".", EstadoPrueba.FAILED, fatal);
        }

    }

    /*************************************************************************************************
     *El metodo "formatearRutTandem", recibe como parametro el rut del usuario, el cual se setea para
     * usarlos en las querys de tandem. Añade ceros al inicio del rut para realizar consultas SQL, y su longitud es de '10'.
     ************************************************************************************************/
    public static String formatearRutTandem(String rut) {
        String rutFormateado;
        rutFormateado = rut.replace(".", "");
        rutFormateado = rutFormateado.substring(0, rutFormateado.length() - 2);
        return right("0000000000" + rutFormateado, 10);
    }

    public static boolean visualizarObjetoScroll(WebElement objeto, int segundos) {
        try {
            System.out.println("Buscamos el objeto:" + objeto + ", esperamos " + segundos + " segundos, hasta que aparezca.");
            WebDriverWait wait = new WebDriverWait( DriverContext.getDriver(), (long) segundos);
            wait.until( ExpectedConditions.visibilityOf(objeto));
            System.out.println("Se encontró objeto:" + objeto + ", se retorna true.");
            Utils.scrollFocusAlElemento(objeto);
            return true;
        } catch (Exception var3) {
            System.out.println("No se encontró objeto:" + objeto + ", se retorna false.");
            return false;
        }
    }

    public static boolean visualizarObjetoMouse(WebElement objeto, int segundos) {
        try {
            System.out.println("Buscamos el objeto:" + objeto + ", esperamos " + segundos + " segundos, hasta que aparezca.");
            WebDriverWait wait = new WebDriverWait( DriverContext.getDriver(), (long) segundos);
            wait.until( ExpectedConditions.visibilityOf(objeto));
            System.out.println("Se encontró objeto:" + objeto + ", se retorna true.");
            Utils.mouseFocusElemento(objeto);
            return true;
        } catch (Exception var3) {
            System.out.println("No se encontró objeto:" + objeto + ", se retorna false.");
            return false;
        }
    }

    public static boolean visualizarObjetoScroll(int maxPixelPantalla, WebElement objeto, int segundos, SwipeDirection direccion, String nombreElemento, WebElement iFrame) {
        boolean elementoEncontrado = false;
        try {
            System.out.println("\nBuscamos el objeto:" + objeto + ", esperamos " + segundos + " segundos, hasta que aparezca.");

            int contador = 0, cantidadPixel = 150;
            boolean validaCambioDireccion = false;

            do {
                elementoEncontrado = ControlledActions.visualizarObjeto(objeto, segundos);
                if (elementoEncontrado) {
                    System.out.println("\n[ControlledActions] - visualizarObjetoScroll, Se encuentra elemento: " + nombreElemento);
                } else {
                    System.out.println("\n[ControlledActions] - visualizarObjetoScroll, se hace scroll para buscar elemento: " + nombreElemento + " intento numero:" + contador);

                    switch (direccion) {
                        case DOWN:
                            scrollVertical(String.valueOf(cantidadPixel), iFrame);
                            break;
                        case UP:
                            scrollVertical("-" + cantidadPixel, iFrame);
                            break;
                        case LEFT:
                            scrollHorizontal("-" + cantidadPixel, iFrame);
                            break;
                        case RIGHT:
                            scrollHorizontal(String.valueOf(cantidadPixel), iFrame);
                            break;
                        default:
                            ////reporte("[ControlledActions] - visualizarObjetoScroll", "ERROR: No se existe la opción '" + direccion + "'.", EstadoPrueba.FAILED, true);

                    }
                    if(!validaCambioDireccion) {
                        cantidadPixel = cantidadPixel + 150;
                    }else{
                        cantidadPixel = cantidadPixel - 150;

                    }
                    contador++;
                }
                if(cantidadPixel >= maxPixelPantalla){
                    validaCambioDireccion = true;
                    switch (direccion) {
                        case DOWN:
                            direccion = SwipeDirection.UP;
                            break;
                        case UP:
                            direccion = SwipeDirection.DOWN;
                            break;
                        case LEFT:
                            direccion = SwipeDirection.RIGHT;
                            break;
                        case RIGHT:
                            direccion = SwipeDirection.LEFT;
                            break;
                    }
                }
            } while (!elementoEncontrado && cantidadPixel > 0);

        } catch (NoSuchElementException var5) {
            System.out.println("[ControlledActions] - visualizarObjetoScroll, No se encuentra elemento falla el meotodo.");
            ////reporte("Error al hacer click a:" + nombreElemento, "[ControlledActions] Error: El elemento '" + nombreElemento + "' no fue encontrado: " + var5.getMessage(), EstadoPrueba.FAILED, true);
        }
        return elementoEncontrado;
    }

    /**************************************************************************************************
     * El metodo "evidencia" captura la imagen del paso visualizadoque le indiquemos,
     * para esto recibe el parametro element(Mensaje asociadada a la acción), driver).
     **************************************************************************************************/
    public static boolean evidencia(String strFuncionalidad, String strPasoCaso){
        String rutaReporte = " ";
        rutaReporte = System.getProperty("user.dir")+"\\reportes\\"+strPasoCaso+".png";
        File scrFile = ((TakesScreenshot)DriverContext.getDriver()).getScreenshotAs(OutputType.FILE);
        try{
            FileUtils.copyFile(scrFile, new File(rutaReporte));
            System.out.println("[ControlledActions] - Evidencia, Captura " + strFuncionalidad);
            return true;
        }catch (IOException e) {
            e.printStackTrace();
            System.out.println("[ControlledActions] - Evidencia, No capturo " + strFuncionalidad);
            return false;

        }
    }
}