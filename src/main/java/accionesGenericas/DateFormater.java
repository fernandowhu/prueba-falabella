package accionesGenericas;

public class DateFormater {

    public static String getNumberFromMonth(String mesString){
        String mes;
        switch (mesString) {
            case   "Enero":  mes ="01" ;
                break;
            case "Febrero":  mes  = "02";
                break;
            case "Marzo":  mes = "03";
                break;
            case "Abril":  mes = "04";
                break;
            case "Mayo":  mes ="05" ;
                break;
            case "Junio":  mes = "06";
                break;
            case "Julio":  mes = "07";
                break;
            case "Agosto":  mes = "08";
                break;
            case "Septiembre":  mes = "09";
                break;
            case "Octubre": mes ="10" ;
                break;
            case "Noviembre": mes = "11";
                break;
            case "Diciembre": mes = "12";
                break;
            default: mes = "Invalido";
                break;
        }
        return mes;
    }
    public static String addZeroToDayNumber(String day){
        if (day.length()<2){
            return "0"+day;
        }else{
            return day;
        }
    }
}
