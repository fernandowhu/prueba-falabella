package accionesGenericas;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {
    public static Properties readFromConfig(String nameFile){
        String propFileName=nameFile;
        Properties properties=new Properties();
        InputStream inputStream= ReadProperties.class.getClassLoader().getResourceAsStream(propFileName);
        if (inputStream!=null){
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                ////reporte("Read Properties","No se pudo encontrar el archivo properties "+propFileName, EstadoPrueba.FAILED, true);
            }
        }else{
            ////reporte("Read Properties","No se pudo encontrar el archivo properties "+propFileName, EstadoPrueba.FAILED, true);
        }
        return properties;
    }
}
