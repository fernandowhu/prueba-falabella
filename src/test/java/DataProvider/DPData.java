package DataProvider;

import java.lang.reflect.Method;

public class DPData {
    @org.testng.annotations.DataProvider(name = "PruebaProvider")
    public static Object[][] getDataFromDataprovider(Method m) {
        return new Object[][]{
            /*  {"LG"     ,  "10000", "20000"},
                {"SONY"   ,  "30000", "90000"  } */
                {"Samsung", "100000", "200000" }
        };
    }
}