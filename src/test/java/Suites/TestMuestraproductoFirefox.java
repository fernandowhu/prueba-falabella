package Suites;

import Clases.CPA0000Test;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static constants.Constants.AMBIENTE_URL;
import static constants.Navegador.Firefox;

public class TestMuestraproductoFirefox {
    @BeforeMethod
    public void setUp() {
          DriverContext.setUp(Firefox, AMBIENTE_URL);
    }

    @AfterMethod
    public void tearDown() {

            DriverContext.quitDriver();
    }

    @Test
    public void CPA00001paginaCRM() {
        CPA0000Test cpa0000 = new CPA0000Test();
        cpa0000.testProductos();
    }
}
